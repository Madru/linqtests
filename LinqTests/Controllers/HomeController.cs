﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LinqTests.Models;

namespace LinqTests.Controllers
{
    public class HomeController : Controller
    {
        EstudiantesModel con = new EstudiantesModel();

        public ActionResult Index()
        {

            /* List<string> materias = new List<string>()
             {
                 "Matemática",
                 "Algoritmia",
                 "MVC Básico",
                 "Historia del internet",
                 "Use Tools",
                 "LinQ"
             };

             var query1 = from materia in materias where materia.StartsWith("M") select materia;
             ViewBag.res1 = query1;
             var query2 = from materia in materias where materia.EndsWith("a".ToUpper()) || materia.EndsWith("a".ToLower()) select materia;
             ViewBag.res2 = query2;
             var query3 = from materia in materias where materia.EndsWith("q".ToUpper()) || materia.EndsWith("q".ToLower()) select materia;
             ViewBag.res3 = query3;
             */


            var lista = con.AddingEstudiante();
            ViewBag.busqueda = true;
            return View(lista.ToList());
        }
        
        [HttpPost]
        public ActionResult Index(string consulta, int tipo)
        {
            consulta.Trim();

            var query = from registro
                                in con.AddingEstudiante()
                        where
                        registro.Nombre.Contains(consulta) ||
                        registro.Materia.Contains(consulta) ||
                       registro.Promedio.ToString().Contains(consulta)
                        select registro;
            
            switch (tipo)
            {
                case 1:
                    query = from registro
                                in con.AddingEstudiante()
                                where
                                registro.Nombre.Contains(consulta)
                                select registro;
                    ViewBag.busqueda = true;
                    break;
                case 2:
                    query = from registro
                                in con.AddingEstudiante()
                                 where
                                 registro.Materia.Contains(consulta)
                                 select registro;
                    ViewBag.busqueda = true;
                    break;
                case 3:
                    query = from registro
                                in con.AddingEstudiante()
                                 where
                                 registro.Promedio.ToString().Contains(consulta)
                                 select registro;
                    ViewBag.busqueda = true;
                    break;
                default:
                    ViewBag.busqueda = false;
                    return View();
            }

            return View(query.ToList());

        }
    }
}