﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LinqTests.Models
{
    public class EstudiantesModel
    {
        public string Carnet { get; set; }
        public string Nombre { get; set; }
        public string Materia { get; set; }
        public double Promedio { get; set; }
        public DateTime Fecha { get; set; }



        public List<EstudiantesModel> AddingEstudiante()
        {

            var est1 = new EstudiantesModel {
                  Carnet = "C1",
                  Nombre = "Ana",
                  Promedio = 9.2,
                  Fecha = DateTime.Now,
                  Materia = "Base de Datos"
            };
            var est2 = new EstudiantesModel
            {
                Carnet = "C2",
                Nombre = "Carlos",
                Promedio = 6.7,
                Fecha = DateTime.Now,
                Materia = "Redes"
            };
            var est3 = new EstudiantesModel
            {
                Carnet = "C3",
                Nombre = "Rosa",
                Promedio = 8.5,
                Fecha = DateTime.Now,
                Materia = "Linq" 
            };
            var est4 = new EstudiantesModel
            {
                Carnet = "C1",
                Nombre = "Marcos",
                Promedio = 9.2,
                Fecha = DateTime.Now,
                Materia = "Algoritmia"
            };
            var est5 = new EstudiantesModel
            {
                Carnet = "C1",
                Nombre = "Laura",
                Promedio = 9.2,
                Fecha = DateTime.Now,
                Materia = "Lógica"
            };

            

            return new List<EstudiantesModel>() {est1,est2,est3,est4,est5};
        }
    }
}